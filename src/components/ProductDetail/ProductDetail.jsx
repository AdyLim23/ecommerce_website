import React,{useState,useEffect} from 'react';
import {Grid,Paper,Typography,Button} from '@material-ui/core';

import {commerce} from '../../lib/commerce';
import useStyle from './styles';
import { Bounce } from "react-activity";

const ProductDetail = ({match,history}) => {
    const classes = useStyle();
    const [productDetail,setProductDetail] = useState({})
    const [quantity,setQuantity] = useState(1);
    const [cart,setCart] = useState({});

    //Method1 -Get Params 
    //direct use the match and put it inside the {} ,then match.params
    
    //Method2 -useParams hook ,etc
    const {id} = match.params
    console.log("ID:",id)
    const fetchProductDetail = async()=>{
        const response = await commerce.products.retrieve(id)
        setProductDetail(response)
    }
    
    const refreshPage = ()=>{
        window.location.reload();
     }

    const handleAddToCart = async (productID,quantity) =>{
        const item = await commerce.cart.add(productID,quantity)
        setCart(item.cart)
        history.push("/")
        refreshPage()
        // console.log("Cart:",cart)
    }

    useEffect(() => {
        fetchProductDetail()
    },[]);
    console.log("Product:",productDetail)
    const quanti = quantity;
    console.log(quanti)

    const addHandler = () =>{
        const quantity = quanti+1;
        setQuantity(quantity)
    }

    const subHandler = () =>{
        const quantity = quanti-1;
        setQuantity(quantity)
    }

    //empty object check method{}
    if (Object.keys(productDetail).length === 0) return(
        <Bounce style={{justifyContent:"center", display: 'flex'}} color="#727981" size={32} speed={1} animating={true} />
    )

    return (
        <div>
            <div className={classes.toolbar} />
            <div style={{backgroundColor:"white",height:"50vh"}}>
                <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item style={{padding:"35px"}}>
                        <div>
                        <img className={classes.img} alt="complex" src={productDetail.media.source} height="500px" width="500px" />
                        </div>
                    </Grid>
                    <Grid item xs={12} sm container>
                            <Grid item xs container direction="column" spacing={6}>
                            <Grid item xs>
                                <Typography gutterBottom variant="h6">
                                    <b>{productDetail.name}</b>
                                </Typography>
                                <Typography variant="body1" dangerouslySetInnerHTML={ {__html: productDetail.description} } gutterBottom/>
                                <Typography variant="body1" color="textSecondary" style={{display:"flex"}}>
                                    Quantity :
                                    {(quantity<=1)?(<Button type="button" size="small" disabled>-</Button>):(<Button type="button" size="small" onClick={subHandler}>-</Button>)}
                                    <Typography>{quantity}</Typography>
                                    <Button type="button" size="small" onClick={addHandler}>+</Button>
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                    <Button variant="contained" type="button" color="secondary" onClick={()=>handleAddToCart(id,quantity)}>Add To Cart</Button>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item style={{paddingRight:"5%"}}>
                            <Typography variant="h6" color="secondary">{productDetail.price.formatted_with_symbol}</Typography>
                        </Grid>
                    </Grid>
                </Grid>         
                </div>
            </div>
        </div>
    )
}

export default ProductDetail
      