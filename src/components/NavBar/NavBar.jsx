import React from 'react'
import { AppBar , Toolbar , Menu ,MenuItem ,IconButton ,Badge ,Typography } from '@material-ui/core';
import { ShoppingCart } from '@material-ui/icons';
import { Link,useLocation } from 'react-router-dom';

import logo from "../../images/logo.jpg"
import useStyle from "./styles";

const NavBar = ({cart}) => {
    const classes = useStyle();
    const location = useLocation();

    return (
        <div>
            <AppBar position="fixed" className={classes.appBar} color="inherit">
                <Toolbar>
                    <Typography variant="h6" color="inherit" className={classes.title} component={Link} to="/">
                        <img src={logo} height="25px" className={classes.image}/>
                        <span style={{alignItems:"center" , color:"orange"}}>SHOPPY</span>
                    </Typography>
                    {/* <div className={classes.grow}/> */}

                    {/* && means if condition true just show */}
                    {(location.pathname=="/") &&
                    (<div className={classes.button}>
                        <IconButton color="inherit" component={Link} to="/cart">
                            <Badge color="secondary" badgeContent={cart.total_items}>
                                <ShoppingCart/>
                            </Badge>
                        </IconButton>
                    </div>)}
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default NavBar

